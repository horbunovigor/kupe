(function ($) {
  ("use strict");
  var layout = $(".layout"),
    header = $(".layout__header"),
    footer = $(".layout__footer");
  preloader();
  function preloader() {
    layout.on("click", "a.nav__link", function (event) {
      layout.removeClass("layout_ready-load");
      event.preventDefault();
      var linkLocation = this.href;
      setTimeout(function () {
        window.location = linkLocation;
      }, 500);
    });
    setTimeout(function () {
      layout.addClass("layout_ready-load");
    }, 0);
  }

  if ($(".product__media").length) {
    $(window).on("load resize", function () {
      if ($(".product__slider_primary").hasClass("slick-initialized")) {
        $(".product__slider_primary").slick("unslick");
        $(".product__slider_primary").slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          arrows: false,
          speed: 1000,
          fade: true,
          dots: false,
          asNavFor: ".product__slider_secondary",
        });
      } else {
        $(".product__slider_primary").slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          arrows: false,
          speed: 1000,
          fade: true,
          dots: false,
          asNavFor: ".product__slider_secondary",
        });
      }

      if ($(".product__slider_secondary").hasClass("slick-initialized")) {
        $(".product__slider_secondary").slick("unslick");
        $(".product__slider_secondary").slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          arrows: true,
          vertical: true,
          speed: 1000,
          focusOnSelect: true,
          asNavFor: ".product__slider_primary",
          prevArrow:
            '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M2.4548 16.0605C2.7361 16.3417 3.11756 16.4997 3.5153 16.4997C3.91305 16.4997 4.29451 16.3417 4.5758 16.0605L12.0008 8.63551L19.4258 16.0605C19.7087 16.3337 20.0876 16.4849 20.4809 16.4815C20.8742 16.4781 21.2504 16.3204 21.5285 16.0422C21.8066 15.7641 21.9644 15.3879 21.9678 14.9946C21.9712 14.6013 21.82 14.2224 21.5468 13.9395L13.0613 5.45401C12.78 5.1728 12.3986 5.01483 12.0008 5.01483C11.6031 5.01483 11.2216 5.1728 10.9403 5.45401L2.4548 13.9395C2.1736 14.2208 2.01563 14.6023 2.01563 15C2.01563 15.3978 2.1736 15.7792 2.4548 16.0605Z" fill="#252422"/></svg></div>',
          nextArrow:
            '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M21.5452 7.93955C21.2639 7.65834 20.8824 7.50037 20.4847 7.50037C20.0869 7.50037 19.7055 7.65834 19.4242 7.93955L11.9992 15.3646L4.57419 7.93955C4.29129 7.66632 3.91238 7.51512 3.51909 7.51854C3.12579 7.52196 2.74958 7.67971 2.47146 7.95782C2.19335 8.23594 2.0356 8.61216 2.03218 9.00545C2.02876 9.39875 2.17996 9.77765 2.45319 10.0606L10.9387 18.5461C11.22 18.8273 11.6014 18.9852 11.9992 18.9852C12.3969 18.9852 12.7784 18.8273 13.0597 18.5461L21.5452 10.0605C21.8264 9.77926 21.9844 9.3978 21.9844 9.00005C21.9844 8.6023 21.8264 8.22084 21.5452 7.93955Z" fill="#252422"/></svg></div>',
        });
      } else {
        $(".product__slider_secondary").slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          arrows: true,
          vertical: true,
          speed: 1000,
          focusOnSelect: true,
          asNavFor: ".product__slider_primary",
          prevArrow:
            '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M2.4548 16.0605C2.7361 16.3417 3.11756 16.4997 3.5153 16.4997C3.91305 16.4997 4.29451 16.3417 4.5758 16.0605L12.0008 8.63551L19.4258 16.0605C19.7087 16.3337 20.0876 16.4849 20.4809 16.4815C20.8742 16.4781 21.2504 16.3204 21.5285 16.0422C21.8066 15.7641 21.9644 15.3879 21.9678 14.9946C21.9712 14.6013 21.82 14.2224 21.5468 13.9395L13.0613 5.45401C12.78 5.1728 12.3986 5.01483 12.0008 5.01483C11.6031 5.01483 11.2216 5.1728 10.9403 5.45401L2.4548 13.9395C2.1736 14.2208 2.01563 14.6023 2.01563 15C2.01563 15.3978 2.1736 15.7792 2.4548 16.0605Z" fill="#252422"/></svg></div>',
          nextArrow:
            '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M21.5452 7.93955C21.2639 7.65834 20.8824 7.50037 20.4847 7.50037C20.0869 7.50037 19.7055 7.65834 19.4242 7.93955L11.9992 15.3646L4.57419 7.93955C4.29129 7.66632 3.91238 7.51512 3.51909 7.51854C3.12579 7.52196 2.74958 7.67971 2.47146 7.95782C2.19335 8.23594 2.0356 8.61216 2.03218 9.00545C2.02876 9.39875 2.17996 9.77765 2.45319 10.0606L10.9387 18.5461C11.22 18.8273 11.6014 18.9852 11.9992 18.9852C12.3969 18.9852 12.7784 18.8273 13.0597 18.5461L21.5452 10.0605C21.8264 9.77926 21.9844 9.3978 21.9844 9.00005C21.9844 8.6023 21.8264 8.22084 21.5452 7.93955Z" fill="#252422"/></svg></div>',
        });
      }
    });
  }

  if ($(".gallery__list").length) {
    $(".gallery__list").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      variableWidth: true,
      arrows: true,
      speed: 1000,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M16.0606 21.5462C16.3418 21.2649 16.4998 20.8834 16.4998 20.4857C16.4998 20.0879 16.3418 19.7065 16.0606 19.4252L8.63557 12.0002L16.0606 4.57517C16.3338 4.29227 16.485 3.91336 16.4816 3.52007C16.4782 3.12677 16.3204 2.75055 16.0423 2.47244C15.7642 2.19433 15.388 2.03658 14.9947 2.03316C14.6014 2.02974 14.2225 2.18093 13.9396 2.45417L5.45407 10.9397C5.17287 11.221 5.01489 11.6024 5.01489 12.0002C5.01489 12.3979 5.17287 12.7794 5.45407 13.0607L13.9396 21.5462C14.2209 21.8274 14.6023 21.9854 15.0001 21.9854C15.3978 21.9854 15.7793 21.8274 16.0606 21.5462Z" fill="#252422"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M7.93919 2.45432C7.65798 2.73561 7.5 3.11707 7.5 3.51482C7.5 3.91256 7.65798 4.29402 7.93919 4.57532L15.3642 12.0003L7.93918 19.4253C7.66595 19.7082 7.51476 20.0871 7.51817 20.4804C7.52159 20.8737 7.67934 21.2499 7.95746 21.528C8.23557 21.8062 8.61179 21.9639 9.00508 21.9673C9.39838 21.9707 9.77728 21.8196 10.0602 21.5463L18.5457 13.0608C18.8269 12.7795 18.9849 12.3981 18.9849 12.0003C18.9849 11.6026 18.8269 11.2211 18.5457 10.9398L10.0602 2.45432C9.77889 2.17311 9.39743 2.01514 8.99968 2.01514C8.60194 2.01514 8.22048 2.17311 7.93919 2.45432Z" fill="#252422"/></svg></div>',
    });
  }

  if ($(".production__carousel").length) {
    $(".production__carousel").slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 1000,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M16.0606 21.5462C16.3418 21.2649 16.4998 20.8834 16.4998 20.4857C16.4998 20.0879 16.3418 19.7065 16.0606 19.4252L8.63557 12.0002L16.0606 4.57517C16.3338 4.29227 16.485 3.91336 16.4816 3.52007C16.4782 3.12677 16.3204 2.75055 16.0423 2.47244C15.7642 2.19433 15.388 2.03658 14.9947 2.03316C14.6014 2.02974 14.2225 2.18093 13.9396 2.45417L5.45407 10.9397C5.17287 11.221 5.01489 11.6024 5.01489 12.0002C5.01489 12.3979 5.17287 12.7794 5.45407 13.0607L13.9396 21.5462C14.2209 21.8274 14.6023 21.9854 15.0001 21.9854C15.3978 21.9854 15.7793 21.8274 16.0606 21.5462Z" fill="#252422"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M7.93919 2.45432C7.65798 2.73561 7.5 3.11707 7.5 3.51482C7.5 3.91256 7.65798 4.29402 7.93919 4.57532L15.3642 12.0003L7.93918 19.4253C7.66595 19.7082 7.51476 20.0871 7.51817 20.4804C7.52159 20.8737 7.67934 21.2499 7.95746 21.528C8.23557 21.8062 8.61179 21.9639 9.00508 21.9673C9.39838 21.9707 9.77728 21.8196 10.0602 21.5463L18.5457 13.0608C18.8269 12.7795 18.9849 12.3981 18.9849 12.0003C18.9849 11.6026 18.8269 11.2211 18.5457 10.9398L10.0602 2.45432C9.77889 2.17311 9.39743 2.01514 8.99968 2.01514C8.60194 2.01514 8.22048 2.17311 7.93919 2.45432Z" fill="#252422"/></svg></div>',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }

  if ($(".portfolio__list").length) {
    $(".portfolio__list").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 1000,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M16.0606 21.5462C16.3418 21.2649 16.4998 20.8834 16.4998 20.4857C16.4998 20.0879 16.3418 19.7065 16.0606 19.4252L8.63557 12.0002L16.0606 4.57517C16.3338 4.29227 16.485 3.91336 16.4816 3.52007C16.4782 3.12677 16.3204 2.75055 16.0423 2.47244C15.7642 2.19433 15.388 2.03658 14.9947 2.03316C14.6014 2.02974 14.2225 2.18093 13.9396 2.45417L5.45407 10.9397C5.17287 11.221 5.01489 11.6024 5.01489 12.0002C5.01489 12.3979 5.17287 12.7794 5.45407 13.0607L13.9396 21.5462C14.2209 21.8274 14.6023 21.9854 15.0001 21.9854C15.3978 21.9854 15.7793 21.8274 16.0606 21.5462Z" fill="#252422"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M7.93919 2.45432C7.65798 2.73561 7.5 3.11707 7.5 3.51482C7.5 3.91256 7.65798 4.29402 7.93919 4.57532L15.3642 12.0003L7.93918 19.4253C7.66595 19.7082 7.51476 20.0871 7.51817 20.4804C7.52159 20.8737 7.67934 21.2499 7.95746 21.528C8.23557 21.8062 8.61179 21.9639 9.00508 21.9673C9.39838 21.9707 9.77728 21.8196 10.0602 21.5463L18.5457 13.0608C18.8269 12.7795 18.9849 12.3981 18.9849 12.0003C18.9849 11.6026 18.8269 11.2211 18.5457 10.9398L10.0602 2.45432C9.77889 2.17311 9.39743 2.01514 8.99968 2.01514C8.60194 2.01514 8.22048 2.17311 7.93919 2.45432Z" fill="#252422"/></svg></div>',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    });
  }

  if ($(".products__list_carousel").length) {
    $(".products__list_carousel").each(function () {
      $(this).slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        speed: 1000,
        appendArrows: $(this).closest(".layout__section").find(".section__nav"),
        prevArrow:
          '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M16.0606 21.5462C16.3418 21.2649 16.4998 20.8834 16.4998 20.4857C16.4998 20.0879 16.3418 19.7065 16.0606 19.4252L8.63557 12.0002L16.0606 4.57517C16.3338 4.29227 16.485 3.91336 16.4816 3.52007C16.4782 3.12677 16.3204 2.75055 16.0423 2.47244C15.7642 2.19433 15.388 2.03658 14.9947 2.03316C14.6014 2.02974 14.2225 2.18093 13.9396 2.45417L5.45407 10.9397C5.17287 11.221 5.01489 11.6024 5.01489 12.0002C5.01489 12.3979 5.17287 12.7794 5.45407 13.0607L13.9396 21.5462C14.2209 21.8274 14.6023 21.9854 15.0001 21.9854C15.3978 21.9854 15.7793 21.8274 16.0606 21.5462Z" fill="#252422"/></svg></div>',
        nextArrow:
          '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M7.93919 2.45432C7.65798 2.73561 7.5 3.11707 7.5 3.51482C7.5 3.91256 7.65798 4.29402 7.93919 4.57532L15.3642 12.0003L7.93918 19.4253C7.66595 19.7082 7.51476 20.0871 7.51817 20.4804C7.52159 20.8737 7.67934 21.2499 7.95746 21.528C8.23557 21.8062 8.61179 21.9639 9.00508 21.9673C9.39838 21.9707 9.77728 21.8196 10.0602 21.5463L18.5457 13.0608C18.8269 12.7795 18.9849 12.3981 18.9849 12.0003C18.9849 11.6026 18.8269 11.2211 18.5457 10.9398L10.0602 2.45432C9.77889 2.17311 9.39743 2.01514 8.99968 2.01514C8.60194 2.01514 8.22048 2.17311 7.93919 2.45432Z" fill="#252422"/></svg></div>',
        responsive: [
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });
    });
  }

  if ($(".testimonials__list_carousel").length) {
    $(".testimonials__list_carousel").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 1000,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M16.0606 21.5462C16.3418 21.2649 16.4998 20.8834 16.4998 20.4857C16.4998 20.0879 16.3418 19.7065 16.0606 19.4252L8.63557 12.0002L16.0606 4.57517C16.3338 4.29227 16.485 3.91336 16.4816 3.52007C16.4782 3.12677 16.3204 2.75055 16.0423 2.47244C15.7642 2.19433 15.388 2.03658 14.9947 2.03316C14.6014 2.02974 14.2225 2.18093 13.9396 2.45417L5.45407 10.9397C5.17287 11.221 5.01489 11.6024 5.01489 12.0002C5.01489 12.3979 5.17287 12.7794 5.45407 13.0607L13.9396 21.5462C14.2209 21.8274 14.6023 21.9854 15.0001 21.9854C15.3978 21.9854 15.7793 21.8274 16.0606 21.5462Z" fill="#252422"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M7.93919 2.45432C7.65798 2.73561 7.5 3.11707 7.5 3.51482C7.5 3.91256 7.65798 4.29402 7.93919 4.57532L15.3642 12.0003L7.93918 19.4253C7.66595 19.7082 7.51476 20.0871 7.51817 20.4804C7.52159 20.8737 7.67934 21.2499 7.95746 21.528C8.23557 21.8062 8.61179 21.9639 9.00508 21.9673C9.39838 21.9707 9.77728 21.8196 10.0602 21.5463L18.5457 13.0608C18.8269 12.7795 18.9849 12.3981 18.9849 12.0003C18.9849 11.6026 18.8269 11.2211 18.5457 10.9398L10.0602 2.45432C9.77889 2.17311 9.39743 2.01514 8.99968 2.01514C8.60194 2.01514 8.22048 2.17311 7.93919 2.45432Z" fill="#252422"/></svg></div>',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            appendArrows: $(".testimonials__list_carousel")
              .closest(".layout__section")
              .find(".section__action"),
          },
        },
      ],
    });
  }

  if ($(".certificates__list").length) {
    $(".certificates__list").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      arrows: true,
      speed: 1000,
      prevArrow:
        '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M16.0606 21.5462C16.3418 21.2649 16.4998 20.8834 16.4998 20.4857C16.4998 20.0879 16.3418 19.7065 16.0606 19.4252L8.63557 12.0002L16.0606 4.57517C16.3338 4.29227 16.485 3.91336 16.4816 3.52007C16.4782 3.12677 16.3204 2.75055 16.0423 2.47244C15.7642 2.19433 15.388 2.03658 14.9947 2.03316C14.6014 2.02974 14.2225 2.18093 13.9396 2.45417L5.45407 10.9397C5.17287 11.221 5.01489 11.6024 5.01489 12.0002C5.01489 12.3979 5.17287 12.7794 5.45407 13.0607L13.9396 21.5462C14.2209 21.8274 14.6023 21.9854 15.0001 21.9854C15.3978 21.9854 15.7793 21.8274 16.0606 21.5462Z" fill="#252422"/></svg></div>',
      nextArrow:
        '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M7.93919 2.45432C7.65798 2.73561 7.5 3.11707 7.5 3.51482C7.5 3.91256 7.65798 4.29402 7.93919 4.57532L15.3642 12.0003L7.93918 19.4253C7.66595 19.7082 7.51476 20.0871 7.51817 20.4804C7.52159 20.8737 7.67934 21.2499 7.95746 21.528C8.23557 21.8062 8.61179 21.9639 9.00508 21.9673C9.39838 21.9707 9.77728 21.8196 10.0602 21.5463L18.5457 13.0608C18.8269 12.7795 18.9849 12.3981 18.9849 12.0003C18.9849 11.6026 18.8269 11.2211 18.5457 10.9398L10.0602 2.45432C9.77889 2.17311 9.39743 2.01514 8.99968 2.01514C8.60194 2.01514 8.22048 2.17311 7.93919 2.45432Z" fill="#252422"/></svg></div>',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
          },
        },
      ],
    });
  }

  if ($(".articles__list").length) {
    $(window).on("load resize", function () {
      if ($(window).width() > 768) {
        $(".articles__list").slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          arrows: true,
          speed: 1000,
          prevArrow:
            '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M16.0606 21.5462C16.3418 21.2649 16.4998 20.8834 16.4998 20.4857C16.4998 20.0879 16.3418 19.7065 16.0606 19.4252L8.63557 12.0002L16.0606 4.57517C16.3338 4.29227 16.485 3.91336 16.4816 3.52007C16.4782 3.12677 16.3204 2.75055 16.0423 2.47244C15.7642 2.19433 15.388 2.03658 14.9947 2.03316C14.6014 2.02974 14.2225 2.18093 13.9396 2.45417L5.45407 10.9397C5.17287 11.221 5.01489 11.6024 5.01489 12.0002C5.01489 12.3979 5.17287 12.7794 5.45407 13.0607L13.9396 21.5462C14.2209 21.8274 14.6023 21.9854 15.0001 21.9854C15.3978 21.9854 15.7793 21.8274 16.0606 21.5462Z" fill="#252422"/></svg></div>',
          nextArrow:
            '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M7.93919 2.45432C7.65798 2.73561 7.5 3.11707 7.5 3.51482C7.5 3.91256 7.65798 4.29402 7.93919 4.57532L15.3642 12.0003L7.93918 19.4253C7.66595 19.7082 7.51476 20.0871 7.51817 20.4804C7.52159 20.8737 7.67934 21.2499 7.95746 21.528C8.23557 21.8062 8.61179 21.9639 9.00508 21.9673C9.39838 21.9707 9.77728 21.8196 10.0602 21.5463L18.5457 13.0608C18.8269 12.7795 18.9849 12.3981 18.9849 12.0003C18.9849 11.6026 18.8269 11.2211 18.5457 10.9398L10.0602 2.45432C9.77889 2.17311 9.39743 2.01514 8.99968 2.01514C8.60194 2.01514 8.22048 2.17311 7.93919 2.45432Z" fill="#252422"/></svg></div>',
        });
      } else if ($(".articles__list").hasClass("slick-initialized")) {
        $(".articles__list").slick("unslick");
      }
    });
  }

  if ($(".works__list_carousel").length) {
    $(window).on("load resize", function () {
      if ($(window).width() < 768) {
        $(".works__list_carousel").slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          arrows: true,
          speed: 1000,
          fade: true,
          prevArrow:
            '<div class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M16.0606 21.5462C16.3418 21.2649 16.4998 20.8834 16.4998 20.4857C16.4998 20.0879 16.3418 19.7065 16.0606 19.4252L8.63557 12.0002L16.0606 4.57517C16.3338 4.29227 16.485 3.91336 16.4816 3.52007C16.4782 3.12677 16.3204 2.75055 16.0423 2.47244C15.7642 2.19433 15.388 2.03658 14.9947 2.03316C14.6014 2.02974 14.2225 2.18093 13.9396 2.45417L5.45407 10.9397C5.17287 11.221 5.01489 11.6024 5.01489 12.0002C5.01489 12.3979 5.17287 12.7794 5.45407 13.0607L13.9396 21.5462C14.2209 21.8274 14.6023 21.9854 15.0001 21.9854C15.3978 21.9854 15.7793 21.8274 16.0606 21.5462Z" fill="#252422"/></svg></div>',
          nextArrow:
            '<div class="slick-next"><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none"><path d="M7.93919 2.45432C7.65798 2.73561 7.5 3.11707 7.5 3.51482C7.5 3.91256 7.65798 4.29402 7.93919 4.57532L15.3642 12.0003L7.93918 19.4253C7.66595 19.7082 7.51476 20.0871 7.51817 20.4804C7.52159 20.8737 7.67934 21.2499 7.95746 21.528C8.23557 21.8062 8.61179 21.9639 9.00508 21.9673C9.39838 21.9707 9.77728 21.8196 10.0602 21.5463L18.5457 13.0608C18.8269 12.7795 18.9849 12.3981 18.9849 12.0003C18.9849 11.6026 18.8269 11.2211 18.5457 10.9398L10.0602 2.45432C9.77889 2.17311 9.39743 2.01514 8.99968 2.01514C8.60194 2.01514 8.22048 2.17311 7.93919 2.45432Z" fill="#252422"/></svg></div>',
        });
      } else if ($(".works__list_carousel").hasClass("slick-initialized")) {
        $(".works__list_carousel").slick("unslick");
      }
    });
  }

  // Menu
  navInit();
  function navInit() {
    header.find(".header__burger").on("click", function () {
      $(this).closest(header).toggleClass("layout__header_menu-active");
      header.find(".header__menu").slideToggle(400);
    });
    header.find(".catalog__header").on("click", function () {
      $(this).closest(".header__catalog").toggleClass("header__catalog_active");
      $(this).siblings(".catalog__main").slideToggle(400);
    });

    $(window).on("load resize", function () {
      if ($(window).width() < 768) {
        footer.find(".menu__header").on("click", function () {
          $(this).closest(".menu__item").toggleClass("menu__item_active");
          $(this).siblings(".menu__main").slideToggle(400);
        });
      }
    });
  }
  // SubMenu
  subNavInit();
  function subNavInit() {
    header.find(".nav__item").on("click", function () {
      $(this).toggleClass("nav__item_active");
      $(this).find(".nav__sublist").slideToggle(400);
    });
  }

  /// Scroll functions
  $(window).on("load resize scroll", function () {
    let h = $(window).height();
    scrollHeader(h);
    scrollSection(h);
    scrollImage(h);
  });
  function scrollHeader(h) {
    if ($(window).scrollTop() >= 1) {
      header.addClass("site-header_animation");
    } else {
      header.removeClass("site-header_animation");
    }
  }
  function scrollSection(h) {
    let section = $(".section");
    section.each(function () {
      if ($(window).scrollTop() + h >= $(this).offset().top) {
        $(this).addClass("section_animation");
      }
    });
  }
  function scrollImage(h) {
    // Image initialization
    let img = $("img");
    img.each(function () {
      if (
        $(window).scrollTop() + h >= $(this).offset().top &&
        this.getAttribute("data-src") &&
        this.src !== this.getAttribute("data-src")
      ) {
        this.src = this.getAttribute("data-src");
      }
    });
  }

  // Tabs init
  if ($(".tabs").length) {
    tabsInit();
  }

  function tabsInit() {
    let position,
      tabsBodyItem = $(".tabs__main").find(".tabs__item"),
      tabsActive = "tabs__item_active";
    $(".tabs__header").on("click", ".tabs__item", function () {
      position = $(this).index();
      $(this).addClass(tabsActive).siblings().removeClass(tabsActive);
      tabsBodyItem
        .eq(position)
        .addClass(tabsActive)
        .siblings()
        .removeClass(tabsActive);
    });
  }

  // Accordion init
  if ($(".accordion").length) {
    accordionInit();
  }

  function accordionInit() {
    $(".accordion").on("click", ".accordion__header", function () {
      $(this).closest(".accordion__item").toggleClass("accordion__item_active");
      $(this)
        .closest(".accordion__item")
        .find(".accordion__main")
        .slideToggle(200);
    });
  }

  // Validation & customize form
  if ($("form").length) {
    // select init
    jcf.setOptions("Select", {
      wrapNative: false,
      wrapNativeOnMobile: false,
      fakeDropInBody: false,
      maxVisibleItems: 5,
    });

    jcf.replaceAll();
    $(".mask_phone").mask("8(999) 999-99-99");
    /* Slider */
    if ($(".range").length) {
      rangeInit();
    }
    function rangeInit() {
      $(".range__slider").slider({
        range: true,
        min: 1500,
        max: 10000,
        step: 100,
        values: [3000, 6000],
        slide: function (event, ui) {
          $(".range__input_min").val(ui.values[0]);
          $(".range__input_max").val(ui.values[1]);
        },
      });
      $(".range__input_min").val($(".range__slider").slider("values", 0));
      $(".range__input_max").val($(".range__slider").slider("values", 1));
    }
  }

  // MODAL INIT
  modalInit();
  function modalInit() {
    let modalName;
    // modal show
    $(document).on("click", ".modal-init", function () {
      layout
        .addClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
      modalName = $(this).data("modalname");
      layout.find("." + modalName + "").addClass("modal__layout_active");
    });
    // modal hide
    $(document).mouseup(function (e) {
      if ($(".modal__layout_active").length) {
        var div = $(".modal__layout");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
          modalHide();
        }
      }
    });
    // modal hide
    $(document).on("click", ".modal__action", function () {
      modalHide();
    });
    // modal hide
    $(window).keydown(function (e) {
      if (e.key === "Escape") {
        modalHide();
      }
    });

    function modalHide() {
      layout
        .removeClass("layout_modal-active")
        .find(".modal__layout")
        .removeClass("modal__layout_active");
    }
  }

  // Scroll
  linkScroll();
  function linkScroll() {
    $('a[href^="#"]:not([href="#"])').click(function (e) {
      e.preventDefault();
      var target = $($(this).attr("href"));
      if (target.length) {
        var scrollTo = target.offset().top;
        $("body, html").animate({ scrollTop: scrollTo + "px" }, 800);
      }
    });
  }

  // Sidebar
  sidebarFunction();
  function sidebarFunction() {
    $(".layout__sidebar").on("click", ".sidebar__initial", function () {
      $(".sidebar__initial").toggleClass("sidebar__initial_active");
      $(".sidebar__dropdown").slideToggle(200);
    });
  }

  if ($(".contact__layout").length) {
    mapInit();
  }

  function mapInit() {
    ymaps.ready(init); // когда апи готово, инициализируемя карту
    var customMap; // объявим переменную для карты
    function init() {
      // функция инициализации
      customMap = new ymaps.Map("customMap", {
        // создадим карту выведем ее в див с id="customMap"
        center: [55.76, 37.64], // центра карты
        behaviors: ["default", "scrollZoom"], // скроллинг колесом
        zoom: 10, // масштаб карты
        controls: ["zoomControl", "fullscreenControl"], // элементы управления
      });
      customPlacemark = new ymaps.Placemark(
        [55.752577, 37.632134],
        {},
        {
          iconLayout: "default#image",
          iconImageHref: "img/pin.png", // картинка иконки
          iconImageSize: [48, 48], // размер иконки в пикселях
          iconImageOffset: [-18, -36], // позиция иконки в пикселях(считается от верхнего левого угла)
        }
      );
      // Добавляем метки на карту
      customMap.geoObjects.add(customPlacemark);
    }
  }
})(jQuery);
